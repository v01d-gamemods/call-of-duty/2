from dearpygui import dearpygui as dpg
from trainerbase.gui.helpers import add_components, simple_trainerbase_menu
from trainerbase.gui.injections import CodeInjectionUI
from trainerbase.gui.teleport import TeleportUI

from injections import infinite_ammo, infinite_grenades, infinite_health, rapid_fire
from teleport import tp


@simple_trainerbase_menu("Call of Duty® 2", 580, 250)
def run_menu():
    with dpg.tab_bar():
        with dpg.tab(label="Main"):
            add_components(
                CodeInjectionUI(infinite_health, "Infinite Health", "F1"),
                CodeInjectionUI(infinite_ammo, "Infinite Ammo", "F2"),
                CodeInjectionUI(infinite_grenades, "Infinite Grenades", "F3"),
                CodeInjectionUI(rapid_fire, "Rapid Fire", "F4"),
            )

        with dpg.tab(label="Teleport"):
            add_components(TeleportUI(tp))
