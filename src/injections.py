from trainerbase.codeinjection import AllocatingCodeInjection, CodeInjection
from trainerbase.process import pm


rapid_fire = CodeInjection(
    pm.base_address + 0xDEA9E,
    b"\x90" * 3,
)

infinite_ammo = AllocatingCodeInjection(
    pm.base_address + 0xDED85,
    """
        mov dword [esi + eax * 4 + 0x318], 999
    """,
    original_code_length=7,
)

infinite_grenades = AllocatingCodeInjection(
    pm.base_address + 0xDF301,
    """
        mov dword [eax + ecx * 4 + 0x318], 999
    """,
    original_code_length=7,
)

infinite_health = AllocatingCodeInjection(
    pm.base_address + 0x1283FA,
    f"""
        cmp ebp, {pm.base_address + 0x171603C - 0x13C}  ; If player HP
        je skip

        mov [ebp + 0x13C], eax

        skip:
        test ebx,ebx
    """,
    original_code_length=8,
)
